local Map = {}
local constants = require("constants")
local blockFont = love.graphics.newFont(constants.BLOCK_TEXT_SIZE * constants.TEXT_SCALE)

local NUMBER_COLOR = {
    [1] = {0, 0.95, 0, 1},
    [2] = {0, 0, 1, 1},
    [3] = {0.5, 0.5, 0, 1},
    [4] = {0.75, 0.25, 0, 1},
    [5] = {1, 0, 0, 1},
    [6] = {1, 0, 1, 1},
    [7] = {0, 1, 1, 1},
}
local lose_sound = love.audio.newSource ("sounds/You_Lose_Sound_Effect.mp3", "static")
local win_sound = love.audio.newSource ("sounds/Epic_Win_Sound_Effects.mp3", "static")
local click_sound = love.audio.newSource ("sounds/clic.mp3", "static")
local click2_sound = love.audio.newSource ("sounds/clic2.mp3", "static") 
function Map.create(x_size, y_size)
    math.randomseed(os.time())
    
    local map = {}
    -- 1 Not Visible
    -- 2 Not visible with flag
    -- 3 Visible without mine
    -- 4 Visible with mine
    -- 5 Visible with pressed mine
    map.grid = {}

    -- 0 No mine
    -- 1 Mine
    map.grid_mines = {}
    map.grid_neighbours = {}

    map.x_size = x_size or 0
    map.y_size = y_size or 0
    map.mine_image = love.graphics.newImage("images/mine_24x24.png")
    map.flag_image = love.graphics.newImage("images/flag_24x24.png")
    map.time_since_last_input = 10

    for y = 1, y_size do
        map.grid[y] = {}
        map.grid_mines[y] = {}
        map.grid_neighbours[y] = {}
        for x = 1, x_size do
            map.grid[y][x] = 1
            map.grid_mines[y][x] = 0
        end
    end

    -- Sort the mines in the map
    local total_mines = 0
    local needed_mines = x_size * y_size * constants.MINES_PERCENT
    while total_mines <= needed_mines do
        local x = math.random(x_size)
        local y = math.random(y_size)
        if map.grid_mines[y][x] == 0 then
            map.grid_mines[y][x] = 1
            total_mines = total_mines + 1
        end
    end

    function map:has_user_won()
        for y = 1, y_size do
            for x = 1, x_size do
                if self.grid_mines[y][x] == 0 and self.grid[y][x] ~= 3 then
                    return false
                end
            end
        end

        return true
    end

    function map.get_neighbors_position(x, y)
        local neighbors_positions = {}
        local possible_positions = {
            {x + 1, y},
            {x, y + 1},
            {x - 1, y},
            {x, y - 1},
            {x + 1, y + 1},
            {x - 1, y - 1},
            {x + 1, y - 1},
            {x - 1, y + 1}
        }

        for i, position in ipairs(possible_positions) do
            if position[1] >= 1 and position[1] <= constants.X_GRID_SIZE
                and position[2] >= 1 and position[2] <= constants.Y_GRID_SIZE then
                table.insert(neighbors_positions, position)
            end
        end

        return neighbors_positions
    end

    function map:display_all_mines()
        for y = 1, y_size do
            for x = 1, x_size do
                if self.grid_mines[y][x] == 1 and self.grid[y][x] == 1 then
                    map.grid[y][x] = 4
                end
            end
        end
    end

    function map:get_mines_in_neighbourhood(x, y)
        if self.grid_mines[y][x] == 1 then
            return -1
        end

        local positions_to_check = self.get_neighbors_position(x, y)

        local total_mines_in_neighbourhood = 0
        for i, position in ipairs(positions_to_check) do
            if self.grid_mines[position[2]][position[1]] == 1 then
                total_mines_in_neighbourhood = total_mines_in_neighbourhood + 1
            end
        end

        return total_mines_in_neighbourhood
    end

    -- Calculate the neighbour mines of each position
    for y = 1, y_size do
        for x = 1, x_size do
            map.grid_neighbours[y][x] = map:get_mines_in_neighbourhood(x, y)
        end
    end

    map.draw_by_state = {
        [1] = function(x, y)
            love.graphics.setColor(0.6, 0.6, 0.6)
            love.graphics.rectangle(
                "fill",
                x + constants.TILE_MARGIN,
                y + constants.TILE_MARGIN,
                constants.TILE_WIDTH - constants.TILE_MARGIN * 2,
                constants.TILE_HEIGHT - constants.TILE_MARGIN * 2
            )

            love.graphics.setColor(0.8, 0.8, 0.8)
            love.graphics.rectangle(
                "fill",
                x + constants.TILE_MARGIN * 2,
                y + constants.TILE_MARGIN * 2,
                constants.TILE_WIDTH - constants.TILE_MARGIN * 4,
                constants.TILE_HEIGHT - constants.TILE_MARGIN * 4
            )
        end,
        [2] = function(x, y)
            map.draw_by_state[1](x, y)
            love.graphics.draw(map.flag_image, x + 8, y + 4)
        end,
        [3] = function(x, y)
            love.graphics.setColor(0.6, 0.6, 0.6)
            love.graphics.rectangle(
                "fill",
                x + constants.TILE_MARGIN,
                y + constants.TILE_MARGIN,
                constants.TILE_WIDTH - constants.TILE_MARGIN * 2,
                constants.TILE_HEIGHT - constants.TILE_MARGIN * 2
            )
            local map_x = math.floor(x / constants.TILE_WIDTH) + 1
            local map_y = math.floor(y / constants.TILE_HEIGHT) + 1

            if map.grid_neighbours[map_y][map_x] > 0 then
                love.graphics.setColor(NUMBER_COLOR[map.grid_neighbours[map_y][map_x]])
                love.graphics.print(
                    map.grid_neighbours[map_y][map_x],
                    x + constants.TILE_MARGIN * 7,
                    y + constants.TILE_MARGIN * 2,
                    0
                )
            end
        end,
        [4] = function(x, y)
            love.graphics.setColor(0.6, 0.6, 0.6)
            love.graphics.rectangle(
                "fill",
                x + constants.TILE_MARGIN,
                y + constants.TILE_MARGIN,
                constants.TILE_WIDTH - constants.TILE_MARGIN * 2,
                constants.TILE_HEIGHT - constants.TILE_MARGIN * 2
            )
            love.graphics.draw(map.mine_image, x + 8, y + 4)
        end,
        [5] = function(x, y)
            love.graphics.setColor(0.8, 0, 0)
            love.graphics.rectangle(
                "fill",
                x + constants.TILE_MARGIN,
                y + constants.TILE_MARGIN,
                constants.TILE_WIDTH - constants.TILE_MARGIN * 2,
                constants.TILE_HEIGHT - constants.TILE_MARGIN * 2
            )
            love.graphics.draw(map.mine_image, x + 8, y + 4)
        end
    }

    function map:draw()
        love.graphics.setFont(blockFont)
        for y = 1, self.y_size do
            for x = 1, self.x_size do
                self.draw_by_state[self.grid[y][x]](
                    (x - 1) * constants.TILE_WIDTH,
                    (y - 1) * constants.TILE_HEIGHT
                )
            end
        end
        love.graphics.setColor(1, 1, 1)
    end

    function map:update(dt)
        self.time_since_last_input = self.time_since_last_input + dt

        if self.time_since_last_input >= constants.MOUSECLICK_DELAY then
            if love.mouse.isDown(1) or love.mouse.isDown(2) then
                local x, y = love.mouse.getPosition()
                local map_x = math.floor(x / constants.TILE_WIDTH) + 1
                local map_y = math.floor(y / constants.TILE_HEIGHT) + 1

                if love.mouse.isDown(1) then
                    -- Press a grid space and display whats bellow
                    if self.grid_mines[map_y][map_x] == 0 then
                        if self.grid[map_y][map_x] == 1 or self.grid[map_y][map_x] == 2 then
                            self.grid[map_y][map_x] = 3
                            if math.random(1,2) == 1 then
                                click_sound:play()
                            else
                                click2_sound:play()
                            end

                            -- If a tile with zero neighbours is open display the ones in the neighborhood
                            if self.grid_neighbours[map_y][map_x] == 0 then
                                local neighbors_to_display = self.get_neighbors_position(map_x, map_y)
                                while #neighbors_to_display > 0 do
                                    local tile_position = table.remove(neighbors_to_display)
                                    local _x = tile_position[1]
                                    local _y = tile_position[2]
                                    if self.grid[_y][_x] < 3 then
                                        if self.grid_neighbours[_y][_x] == 0 then
                                            for i, neighbor_pos in ipairs(self.get_neighbors_position(_x, _y)) do
                                                if self.grid[neighbor_pos[2]][neighbor_pos[1]] < 3 then
                                                    table.insert(neighbors_to_display, neighbor_pos)
                                                end
                                            end
                                        end
                                        self.grid[_y][_x] = 3
                                    end
                                end
                            end

                            if self:has_user_won() then
                                gamestate = 2
                                win_sound:play()
                            end
                        end
                    elseif self.grid_mines[map_y][map_x] == 1 then
                        self:display_all_mines()
                        self.grid[map_y][map_x] = 5
                        gamestate = 1
                        lose_sound:play()
                    end
                elseif love.mouse.isDown(2) then
                    -- Toggle the flag
                    if self.grid[map_y][map_x] == 1 then
                        self.grid[map_y][map_x] = 2
                    elseif self.grid[map_y][map_x] == 2 then
                        self.grid[map_y][map_x] = 1
                    end
                end


                self.time_since_last_input = 0
            end
        end
    end

    function map:release()
        renderer:removeRenderer(self)
        gameloop:removeGameloop(self)
    end

    renderer:addRenderer(map)
    gameloop:addGameloop(map)
    return map
end

return Map
