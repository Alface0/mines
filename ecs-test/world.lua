local Entity = require "entity"

local World = {
    entities = {},
    systems = {},
    event_listeners = {}
}

function World:register(system)
    table.insert(self.systems, system)
end

function World:register_event(event, system, callback)
    if not self.event_listeners[event] then
        self.event_listeners[event] = {}
    end

    table.insert(self.event_listeners[event], {system, callback})
end

function World:create()
    local entity = Entity.new()
    table.insert(self.entities, entity)
    return entity
end

function World:update(dt)
    for i = #self.entities, 1, -1 do
        local entity = self.entities[i]
        if entity.remove then
            for _, system in ipairs(self.systems) do
                if system:match(entity) then
                    system:destroy(entity)
                end
            end

            table.remove(self.entities, i)
        else
            for _, system in ipairs(self.systems) do
                if system:match(entity) then
                    if entity.loaded == false then
                        system:load(entity)
                    end

                    system:update(dt, entity)
                end
            end

            entity.loaded = true
        end
    end
end

function World:draw()
    for i = 1, #self.entities do
        local entity = self.entities[i]
        for _, system in ipairs(self.systems) do
            if system:match(entity) then
                system:draw(entity)
            end
        end
    end
end

function World:fire_event(event, entity, data)
    if self.event_listeners[event] then
        for _, listener in ipairs(self.event_listeners[event]) do
            local system = listener[1]
            local callback = listener[2]

            callback(event, system, entity, data or {})
        end
    end
end

return World
