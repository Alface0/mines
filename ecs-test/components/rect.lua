local Component = require "component"

return function(width, height)
    local rect = Component.new("rect")
    rect.width = width
    rect.height = height
    return rect
end