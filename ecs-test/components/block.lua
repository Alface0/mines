local Component = require "component"

return function (state, has_mine, neighbourhood)
    local block = Component.new("block")
    -- 1 Not Visible
    -- 2 Not visible with flag
    -- 3 Visible without mine
    -- 4 Visible with mine
    -- 5 Visible with pressed mine
    block.state = state or 1
    block.has_mine = has_mine or false
    block.mines_in_neighbourhood = neighbourhood or 0
    return block
end