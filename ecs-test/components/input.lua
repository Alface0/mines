local Component = require "component"

return function()
	local input = Component.new("input")
	input.elapsed_time = 0
	return input
end