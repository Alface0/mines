local Component = require "component"

return function (x, y)
    local point = Component.new("point")
    point.x = x
    point.y = y
    return point
end
