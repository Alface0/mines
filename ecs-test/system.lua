return {
    new = function(requires, world)
        assert(type(requires) == "table")
        local system = {
            requires = requires,
            world = world or nil
        }

        function system:match(entity)
            for i = 1, #self.requires do
                if entity:get(self.requires[i]) == nil then
                    return false
                end
            end

            return true
        end

        function system:fire_event(event, entity, data)
            self.world:fire_event(event, entity, data or {})
        end


        function system:load(entity)end
        function system:update(dt, entity)end
        function system:draw(entity)end
        function system:ui_draw(entity)end
        function system:destroy(entity)end

        return system
    end
}
