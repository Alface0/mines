World = require "world"

-- Systems
local System = require "system"
local RectClickDetectionSystem = require "systems/rect_click_detection_system"
local BlockRenderSystem = require "systems/block_render_system"
local BlockClickDetectionSystem = require "systems/block_click_detection_system"
local EndGameCheckSystem = require "systems/end_game_check_system"
local BlockExpandingSystem = require "systems/block_expanding_system"

-- Components
local Component = require "component"
local Rect = require "components/rect"
local Point = require "components/point"
local Block = require "components/block"
local Input = require "components/input"

constants = require "constants"
textFont = love.graphics.newFont(8 * constants.TEXT_SCALE)

function get_neighbors_position(x, y)
    local neighbors_positions = {}
    local possible_positions = {
        {x + 1, y},
        {x, y + 1},
        {x - 1, y},
        {x, y - 1},
        {x + 1, y + 1},
        {x - 1, y - 1},
        {x + 1, y - 1},
        {x - 1, y + 1}
    }

    for _, position in ipairs(possible_positions) do
        if position[1] >= 1 and position[1] <= constants.X_GRID_SIZE
            and position[2] >= 1 and position[2] <= constants.Y_GRID_SIZE then
            table.insert(neighbors_positions, position)
        end
    end

    return neighbors_positions
end

local function create_map_blocks()
    local map_blocks = {}
    for y = 1, constants.Y_GRID_SIZE do
        for x = 1, constants.X_GRID_SIZE do
            local entity = World:create()
            entity:add(Point((x-1) * constants.TILE_WIDTH, (y - 1) * constants.TILE_HEIGHT))
            entity:add(Rect(constants.TILE_WIDTH, constants.TILE_HEIGHT))
            entity:add(Block(1, false, 0))
            entity:add(Input())
            map_blocks[#map_blocks + 1] = entity
        end
    end

    -- Sort the mines randomly across the blocks
    local total_mines = 0
    local needed_mines = constants.X_GRID_SIZE * constants.Y_GRID_SIZE * constants.MINES_PERCENT
    while total_mines <= needed_mines do
        local random_index = math.random(#map_blocks)
        local random_block = map_blocks[random_index]:get("block")
        if not random_block.has_mine then
            random_block.has_mine = true
            total_mines = total_mines + 1
        end
    end

    -- Check the number of mines in the neighbourhood
    for y = 1, constants.Y_GRID_SIZE do
        for x = 1, constants.X_GRID_SIZE do
            local block = map_blocks[(y-1) * constants.X_GRID_SIZE + x]:get("block")
            local neighbors_positions = get_neighbors_position(x, y)
            for _, position in ipairs(neighbors_positions) do
                local neighbour_block = map_blocks[(position[2]-1) * constants.X_GRID_SIZE + position[1]]:get("block")
                if neighbour_block.has_mine then
                    block.mines_in_neighbourhood = block.mines_in_neighbourhood + 1
                end
            end
        end
    end
end

function love.load()
    love.graphics.setBackgroundColor(0.9, 0.9, 0.9)
    love.graphics.setFont(textFont)

    World:register(BlockRenderSystem())
    World:register(RectClickDetectionSystem(World, 0.2))
    World:register(BlockClickDetectionSystem(World))
    World:register(EndGameCheckSystem(World))
    World:register(BlockExpandingSystem(World))
    create_map_blocks()
end

function love.update(dt)
    World:update(dt)
end

function love.draw ()
    World:draw()
end

function love.keypressed(key)
    if key == "r" then
        World.entities = {}
        create_map_blocks()
        World:fire_event("restart_game", nil)
    end
end
