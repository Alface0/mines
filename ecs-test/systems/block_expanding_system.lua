local System = require "system"

return function(world)
	local system = System.new({"block", "point"}, world or nil)

    system.world:register_event("block_opened", system, function(event, _system, entity)
        print("Checking for expansion")
        local clicked_point = entity:get("point")
        local map_x = clicked_point.x / constants.TILE_WIDTH + 1
        local map_y = clicked_point.y / constants.TILE_HEIGHT + 1

        local clicked_block = entity:get("block")

        if (clicked_block.state == 1 or clicked_block.state == 3) and clicked_block.mines_in_neighbourhood == 0 then
            local neighbour_positions = get_neighbors_position(map_x, map_y)

            for i = 1, #_system.world.entities do
                local current_entity = _system.world.entities[i]

                if _system:match(current_entity) then
                    local point = current_entity:get("point")
                    local block = current_entity:get("block")
                    local block_x = point.x / constants.TILE_WIDTH + 1
                    local block_y = point.y / constants.TILE_HEIGHT + 1

                    for _, position in ipairs(neighbour_positions) do
                        if block_x == position[1] and block_y == position[2] then
                            if block.state == 1 then
                                block.state = 3

                                if block.mines_in_neighbourhood == 0 then
                                    World:fire_event("block_opened", current_entity)
                                end
                            end
                        end
                    end
                end
            end
        end
    end)

	return system
end
