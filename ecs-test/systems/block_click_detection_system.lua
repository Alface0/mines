local System = require "system"

return function(world)
	local system = System.new({"block"}, world or nil)
    system.game_ended = false

    system.world:register_event("entity_pressed", system, function(_, _, entity, data )
        if not system.game_ended then
            local block = entity:get("block")
            local key = data.key

            if key == 1 and (block.state == 1 or block.state == 2) then
                if block.has_mine then
					block.state = 5
					system:fire_event("mine_pressed", entity)
				else
					block.state = 3
					system:fire_event("block_opened", entity)
				end
            elseif key == 2 then
				if block.state == 1 then
					block.state = 2
				elseif block.state == 2 then
					block.state = 1
				end
			end
        end
    end)

    system.world:register_event("game_lost", system, function()
        system.game_ended = true
    end)

    system.world:register_event("restart_game", system, function()
        system.game_ended = false
    end)

	return system
end
