local System = require "system"

local function trigger_defeat(event, system, entity)
	print("Trigger Defeat")
	for i = 1, #system.world.entities do
		local current_entity = system.world.entities[i]
		if system:match(current_entity) then
			local block = current_entity:get("block")
			if block.has_mine and block.state == 1 then
				block.state = 4
			end
		end

		local block = entity:get("block")
		block.state = 5
        World:fire_event("game_lost", entity)
	end
end

local function check_victory(event, system, entity)
	print("Check Victory")
end

return function(world)
	local system = System.new({"block"}, world)
	system.world:register_event("mine_pressed", system, trigger_defeat)
	system.world:register_event("block_opened", system, check_victory)
	return system
end
