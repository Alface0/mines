local System = require "system"
local mine_image = love.graphics.newImage("images/mine_24x24.png")
local flag_image = love.graphics.newImage("images/flag_24x24.png")
local NUMBER_COLOR = {
    [1] = {0, 1, 0, 1},
    [2] = {0, 0, 1, 1},
    [3] = {0.5, 0.5, 0, 1},
    [4] = {0.75, 0.25, 0, 1},
    [5] = {1, 0, 0, 1},
    [6] = {1, 0, 1, 1},
    [7] = {0, 1, 1, 1},
}

-- 1 Not Visible
local function draw_not_visible(point, rect, block)
	love.graphics.setColor(0.6, 0.6, 0.6)
    love.graphics.rectangle(
        "fill",
        point.x + constants.TILE_MARGIN,
        point.y + constants.TILE_MARGIN,
        rect.width - constants.TILE_MARGIN * 2,
        rect.height - constants.TILE_MARGIN * 2
    )

    love.graphics.setColor(0.8, 0.8, 0.8)
    love.graphics.rectangle(
        "fill",
        point.x + constants.TILE_MARGIN * 2,
        point.y + constants.TILE_MARGIN * 2,
        rect.width - constants.TILE_MARGIN * 4,
        rect.height - constants.TILE_MARGIN * 4
    )
end

-- 2 Not visible with flag
local function draw_not_visible_with_flag(point, rect, block)
	draw_not_visible(point, rect, block)
    love.graphics.draw(flag_image, point.x + 8, point.y + 4)
end

-- 3 Visible without mine
local function draw_visible_without_mine(point, rect, block)
    love.graphics.setColor(0.6, 0.6, 0.6)
    love.graphics.rectangle(
        "fill",
        point.x + constants.TILE_MARGIN,
        point.y + constants.TILE_MARGIN,
        rect.width - constants.TILE_MARGIN * 2,
        rect.height - constants.TILE_MARGIN * 2
    )

    if block.mines_in_neighbourhood > 0 then
        love.graphics.setColor(NUMBER_COLOR[block.mines_in_neighbourhood])
        love.graphics.print(
            block.mines_in_neighbourhood,
            point.x + constants.TILE_MARGIN * 7,
            point.y + constants.TILE_MARGIN * 2,
            0,
            constants.TEXT_SCALE_X,
            constants.TEXT_SCALE_Y
        )
    end
end

-- 4 Visible with mine
local function draw_visible_with_mine(point, rect, block)
    love.graphics.setColor(0.6, 0.6, 0.6)
    love.graphics.rectangle(
        "fill",
        point.x + constants.TILE_MARGIN,
        point.y + constants.TILE_MARGIN,
        rect.width - constants.TILE_MARGIN * 2,
        rect.height - constants.TILE_MARGIN * 2
    )
    love.graphics.draw(mine_image, point.x + 8, point.y + 4)
end

-- 5 Visible with pressed mine
local function draw_visible_with_pressed_mine(point, rect, block)
    love.graphics.setColor(0.8, 0, 0)
    love.graphics.rectangle(
        "fill",
        point.x + constants.TILE_MARGIN,
        point.y + constants.TILE_MARGIN,
        rect.width - constants.TILE_MARGIN * 2,
        rect.height - constants.TILE_MARGIN * 2
    )
    love.graphics.draw(mine_image, point.x + 8, point.y + 4)
end

return function()
	local system = System.new({"point", "rect", "block"})

	system.draw_by_state = {
	    [1] = draw_not_visible,
	    [2] = draw_not_visible_with_flag,
	    [3] = draw_visible_without_mine,
	    [4] = draw_visible_with_mine,
	    [5] = draw_visible_with_pressed_mine
	}

	function system:draw(entity)
		local point = entity:get("point")
		local rect = entity:get("rect")
		local block = entity:get("block")

        self.draw_by_state[block.state](point, rect, block)
        love.graphics.setColor(1, 1, 1)
	end

	return system
end
