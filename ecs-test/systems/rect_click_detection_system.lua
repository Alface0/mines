local System = require "system"

return function(world, input_delay)
	local system = System.new({"point", "rect", "input"}, world or nil)
    system.input_delay = input_delay or 0.2

	function system.is_rect_pressed(point, rect)
        local x, y = love.mouse.getPosition()
        local key_pressed = nil
        if love.mouse.isDown(1) then
            key_pressed = 1
	    end

        if love.mouse.isDown(2) then
            key_pressed = 2
        end

        if key_pressed then
            return x >= point.x
                and x <= point.x + rect.width
                and y >= point.y
                and y <= point.y + rect.height, key_pressed
        end

	    return false, nil
	end

	function system:update(dt, entity)
		local point = entity:get("point")
		local rect = entity:get("rect")
        local input = entity:get("input")

        local is_pressed, key = self.is_rect_pressed(point, rect)
		if is_pressed and input.elapsed_time >= self.input_delay then
			self:fire_event("entity_pressed", entity, {key=key})
            input.elapsed_time = 0
		end

        input.elapsed_time = input.elapsed_time + dt
    end

	return system
end
