local Gameloop = {}


function Gameloop:create()
	local gameloop = {}

	gameloop.tickers = {}

	function gameloop:addGameloop(object)
		table.insert(self.tickers, object)
	end

	function gameloop:removeGameloop(object)
		for index, value in pairs(self.tickers) do
			if value == object then
		    	table.remove(self.tickers, index)
		    	break
			end
		end
	end

	function gameloop:update(dt)
		for ticker = 1, #self.tickers do
			local object = self.tickers[ticker]
			if object then
				object:update(dt)
			end
		end
	end

	return gameloop
end

return Gameloop