local constants = require("constants")

function love.conf(t)
	t.window.width = constants.MONITOR_WIDTH
	t.window.height = constants.MONITOR_HEIGHT
	t.title = "Minesweeper"
end
