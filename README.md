# Minesweeper

Minesweeper built using Lua and Love2D.

## TODO

* Add timer to the game
* Add title bar with the current grid size
* Add some way to allow the setting of different grid sizes
* Add sound in the multiple game events for feedback
* Add hover effect when passing the mouse in the toggable buttons
* Add time highscores by grid size

## Playing Preview

Check the following video to see the game being played - [link](https://www.youtube.com/watch?v=s9NHgKHpjGk).

## Dependencies

You need to have love2d installed.

* Archlinux - just install love from pacman
```sh
pacman -S love
```

* Debian - Since the default version in the debian repositories is the 0.9, you need to download the 11 from the [love2D](https://bitbucket.org/rude/love/downloads/) website.

## Running the game

The game was built with love2D version 11.2, so to run the game go to the project folder and run the following command.
```sh
love .
```