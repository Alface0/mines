local Map = require("map")
local Renderer = require("tools/renderer")
local Gameloop = require("tools/gameloop")
local constants = require("constants")
local map

renderer = Renderer.create()
gameloop = Gameloop.create()

-- 0 Running
-- 1 Lose
-- 2 Won
gamestate = 0
textFont = love.graphics.newFont(constants.TEXT_SIZE * constants.TEXT_SCALE)
local ambient_sound = love.audio.newSource ("sounds/ambient.mp3", "stream")
ambient_sound:setLooping(true)

function love.load()
    love.graphics.setBackgroundColor(0.9, 0.9, 0.9)
    map = Map.create(constants.X_GRID_SIZE, constants.Y_GRID_SIZE)
    ambient_sound: setVolume (0.5)
    ambient_sound: play() 
end

function love.update(dt)
    if gamestate == 0 then
        gameloop:update(dt)
    end
end

function love.draw()
    renderer:draw()

    love.graphics.setFont(textFont)
    love.graphics.setColor(0, 0, 0)
    if gamestate == 1 then
        love.graphics.printf(
            "You Lose",
            0,
            constants.MONITOR_HEIGHT / 4,
            constants.MONITOR_WIDTH,
            "center"
        )
    elseif gamestate == 2 then
        love.graphics.printf(
            "You Won",
            0,
            constants.MONITOR_HEIGHT / 4,
            constants.MONITOR_WIDTH,
            "center"
        )
    end
    love.graphics.setColor(1, 1, 1)
end

function love.keypressed(key)
   if (gamestate == 1 or gamestate == 2) and key == "r" then
        gamestate = 0
        map:release()
        love.load()
   end
end
